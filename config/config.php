<?php

    define('DEBUG', true);

    define('DB_NAME', 'new-mvc-project');//database name
    define('DB_USER', 'root');// database user
    define('DB_PASSWORD', 'root'); //database password
    define('DB_HOST', 'localhost');// database host

    define('DEFAULT_CONTROLLER', 'HomeController'); //default controller
    define('DEFAULT_LAYOUT', 'app');
    define('PROOT', '/');
    define('SITE_TITLE', 'Anna Borisenko MVC project');