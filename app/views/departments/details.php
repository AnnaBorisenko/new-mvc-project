<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Главная</a></li>
        <li class="breadcrumb-item"><a href="/departments">Отделы</a></li>
        <?php foreach ($vars as $var) {?>
        <li class="breadcrumb-item active" aria-current="page"><?php echo $var["title"]; ?></li>
    </ol>
</nav>
<div class="card col-8 mx-auto">
    <div class="card-header">
        <h1 class="text-center">Отдел <?php echo $var["title"]; ?></h1>
        <a class="btn btn-outline-info" href="/departments">
            <i class="fa fa-arrow-left"></i> Вернуться назад
        </a>
    </div>
    <div class="card-body">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?php echo $var["id"]; ?></td>
                <td><?php echo $var["title"]; ?></td>

            </tr>
            </tbody>
        </table>
    </div>
</div>
<?php } ?>