<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Главная</a></li>
        <li class="breadcrumb-item active" aria-current="page">Отделы</li>
    </ol>
</nav>
<div class="card col-8 mx-auto">
    <div class="card-header">
        <h1 class="text-center">Отделы</h1>
        <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-plus"></i> Добавить новый отдел
        </button>
    </div>
    <div class="card-body">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">название</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($vars as $department) { ?>
                <tr class="pointer">
                    <th onclick="location.href='departments/details/<?php echo $department["id"]; ?>'"
                        scope="row"><?php echo $department["id"]; ?></th>
                    <td onclick="location.href='departments/details/<?php echo $department["id"]; ?>'"><?php echo $department["title"]; ?></td>
                    <td>

                        <a href="departments/delete/<?php echo $department["id"]; ?>"
                           class="btn btn-danger btn-xs float-right" onclick="
                                if(!confirm('Do you really want to delete <?php echo $department["title"]; ?> ?')){
                                return false;}"><i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="departments/add" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Добавить отдел</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group col-lg-12 mb-2">
                        <label for="title" class="col-xs-2 control-label">Название:</label>
                        <div class="col-xs-8">
                            <input type="text" name="title" class="form-control" id="title"
                                   placeholder="Введите название">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </div>
            </form>
        </div>
    </div>
</div>