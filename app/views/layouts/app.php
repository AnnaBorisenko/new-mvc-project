<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $this->_siteTitle; ?></title>
    <link rel="stylesheet" href="/public/css/main.css">
    <link rel="stylesheet" href="/public/css/fonts.css">
    <link rel="stylesheet" href="/public/css/bootstrap.min.css">

    <script defer src="/public/fonts/fontawesome-free/js/brands.js"></script>
    <script defer src="/public/fonts/fontawesome-free/js/solid.js"></script>
    <script defer src="/public/fonts/fontawesome-free/js/fontawesome.js"></script>
    <script src="/public/js/jQuery3-5-1.min.js"></script>
    <script src="/public/js/popper1-16-1.min.js"></script>
    <script src="/public/js/bootstrap4-6-0.min.js"></script>
    <script src="/public/js/main.js"></script>
</head>
<body>
<div class="d-flex flex-column flex-shrink-0 p-3 text-white bg-dark sidebar">

    <ul class="nav nav-pills flex-column mb-auto">
        <li class="nav-item">
            <a href="/" class="nav-link text-white" aria-current="page">
                <i class="fa fa-home mr-2"></i>
                Главная
            </a>
        </li>
        <li>
            <a href="/users" class="nav-link text-white">
                <i class="fa fa-users mr-2"></i>
                Сотрудники
            </a>
        </li>
        <li>
            <a href="/departments" class="nav-link text-white">
                <i class="fa fa-briefcase mr-2"></i>
                Департаменты
            </a>
        </li>

    </ul>
    <hr>
    <div class="dropdown">

    </div>
</div>

<?php echo $content; ?>

</body>
</html>

