<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Главная</a></li>
        <li class="breadcrumb-item active" aria-current="page">Сотрудники</li>
    </ol>
</nav>

<div class="card col-8 mx-auto">
    <div class="card-header">
        <h1 class="text-center">Сотрудники</h1>
          <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-plus"></i> Добавить нового сотрудника
        </button>
    </div>
    <div class="card-body">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Имя</th>
                <th scope="col">Email</th>
                <th scope="col">Телефон</th>
                <th scope="col">Департамент</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($vars["users"] as $user=>$item) {?>
            <tr class="pointer">
                <th scope="row"><?php echo $item["id"]; ?></th>
                <td  onclick="location.href='users/details/<?php echo $item["id"]; ?>'"><?php echo $item["name"]; ?></td>
                <td  onclick="location.href='users/details/<?php echo $item["id"]; ?>'"><?php echo $item["email"]; ?></td>
                <td  onclick="location.href='users/details/<?php echo $item["id"]; ?>'"><?php echo $item["phone"]; ?></td>
                <td  onclick="location.href='users/details/<?php echo $item["id"]; ?>'"><?php echo $item["department_id"]; ?></td>
                <td>
                    <a href="users/delete/<?php echo $item["id"]; ?>"
                       class="btn btn-danger btn-xs float-right" onclick="
                            if(!confirm('Do you really want to delete <?php echo $item["name"]; ?> ?')){
                            return false;}"><i class="fa fa-times"></i>
                    </a>
                </td>
            </tr>
            <?php } ?>

            </tbody>
        </table>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="users/add" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Добавить нового сотрудника</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group col-lg-12 mb-2">
                        <label for="name" class="col-xs-2 control-label">Имя:</label>
                        <div class="col-xs-8">
                            <input type="text" name="name" class="form-control" id="name"
                                   placeholder="Введите имя сотрудника">
                        </div>
                    </div>
                    <div class="form-group col-lg-12 mb-2">
                        <label for="email" class="col-xs-2 control-label">Email:</label>
                        <div class="col-xs-8">
                            <input type="email" name="email" class="form-control" id="email"
                                   placeholder="Введите email сотрудника">
                        </div>
                    </div>
                    <div class="form-group col-lg-12 mb-2">
                        <label for="phone" class="col-xs-2 control-label">Телефон:</label>
                        <div class="col-xs-8">
                            <input type="text" name="phone" class="form-control" id="phone"
                                   placeholder="Введите телефон сотрудника">
                        </div>
                    </div>
                    <div class="form-group col-lg-12 mb-2">
                        <label for="comment" class="col-xs-2 control-label">Коментарий:</label>
                        <div class="col-xs-8">
                            <input type="text" name="comment" class="form-control" id="comment"
                                   placeholder="Введите коментарий">
                        </div>
                    </div>
                    <div class="form-group col-lg-12 mb-2">
                        <label for="department_id" class="col-xs-2 control-label">Выберите департамент:</label>
                        <select name="department_id" id="department_id" class="form-control">
                            <option value="">Выберите департамент</option>
                            <?php foreach ($vars["departments"] as $department=>$val) { ?>

                            <option value="<?php echo $val["id"]; ?>"><?php echo $val["title"]; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>