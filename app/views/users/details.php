<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Главная</a></li>
        <li class="breadcrumb-item"><a href="/users">Сотрудники</a></li>
        <?php foreach ($vars as $var) {?>
        <li class="breadcrumb-item active" aria-current="page"><?php echo $var["name"]; ?></li>
    </ol>
</nav>
<div class="card col-8 mx-auto">
    <div class="card-header">
        <h1 class="text-center">Сотрудник <?php echo $var["name"]; ?></h1>
        <a class="btn btn-outline-info" href="/users">
            <i class="fa fa-arrow-left"></i> Вернуться назад
        </a>
    </div>
    <div class="card-body">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Имя</th>
                <th scope="col">Email</th>
                <th scope="col">Телефон</th>
                <th scope="col">Коментарий</th>
                <th scope="col">Департамент</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?php echo $var["id"]; ?></td>
                <td><?php echo $var["name"]; ?></td>
                <td><?php echo $var["email"]; ?></td>
                <td><?php echo $var["phone"]; ?></td>
                <td><?php echo $var["comment"]; ?></td>
                <td><?php echo $var["department_id"]; ?></td>

            </tr>
            </tbody>
        </table>
    </div>
</div>
<?php } ?>