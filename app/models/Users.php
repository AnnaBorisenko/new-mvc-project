<?php


namespace app\models;

use PDO;
use core\Model;

class Users extends Model
{
    public $id, $name, $phone, $email, $comment, $department_id;
    public function __construct()
    {
        $table = 'users';
        parent::__construct($table);
    }

    public static $addValidation = [
        'name' => [
            'display' => 'name',
            'required' => true,
            'min' => 4,
        ],
        'email' => [
            'display' => 'email',
            'required' => true,
            'min' => 4,
        ],
        'phone' => [
            'display' => 'phone',
            'required' => true,
            'min' => 4,
        ],
        'comment' => [
            'display' => 'comment',

            'min' => 4,
        ],
        'department_id' => [
            'display' => 'department_id',
            'required' => true,
        ],
    ];

    public function findByUserName($userName)
    {
        return $this->findFirst(['conditions' => 'usrename = ?', 'bind' => [$userName]]);
    }

    public function findAllUsers()
    {
        $usersQ = $this->query("SELECT * FROM users", [])->results();
        return json_decode(json_encode($usersQ), true);
    }

    public function findById($id)
    {
        $sql = "SELECT * FROM users WHERE id={$id["0"]}";
        $userQ = $this->query($sql, [])->results();

        return json_decode(json_encode($userQ), true);
    }

    public function findDepartment($id)
    {
        return $this->findById($id);
    }

}