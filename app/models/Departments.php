<?php


namespace app\models;


use core\Model;

class Departments extends Model
{
    public $id, $title;

    public function __construct()
    {
        $table = 'departments';
        parent::__construct($table);

    }

    public static $addValidation = [
        'title' => [
            'display' => 'title',
            'required' => true,
            'unique',
            'min' => 4,
        ]
    ];

    public function findAllDepartments()
    {
        $departmentsQ = $this->query("SELECT * FROM departments", [])->results();
        return json_decode(json_encode($departmentsQ), true);
    }


    public function findById($id)
    {

        $sql = "SELECT * FROM departments WHERE id={$id["0"]}";
        $departmentQ = $this->query($sql, [])->results();

        return json_decode(json_encode($departmentQ), true);
    }


}