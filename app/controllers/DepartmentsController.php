<?php


namespace app\controllers;


use app\models\Departments;
use core\Controller;
use core\Router;
use core\Validate;

class DepartmentsController extends Controller
{
    public function __construct($controller, $action)
    {
        parent::__construct($controller, $action);
        $this->load_model('Departments');
        $this->view->setLayout('app');

    }
    public function indexAction()
    {
        $departments = $this->DepartmentsModel->findAllDepartments();
        $this->view->render('departments/index', 'Departments page', $departments);
    }

    public function addAction()
    {
        $department = new Departments();
        $validation = new Validate();

        if ($_POST) {
            $department->assign($_POST);
            $validation->check($_POST, Departments::$addValidation);
            if ($validation->passed()) {
                $department->save();
                Router::redirect('/departments');
            }
        }
        $this->view->displayErrors = $validation->displayErrors();
    }

    public function detailsAction($id)
    {
        $department = $this->DepartmentsModel->findById($id);
        $this->view->render('departments/details', 'Department page', $department);
    }

    public function deleteAction($id)
    {
        $this->DepartmentsModel->delete($id["0"]);
        Router::redirect('/departments');
    }

}