<?php


namespace app\controllers;

use app\models\Departments;
use app\models\Users;
use core\Controller;
use core\Router;
use core\Validate;

class UsersController extends Controller
{
    public function __construct($controller, $action)
    {
        parent::__construct($controller, $action);
        $this->load_model('Users');
        $this->view->setLayout('app');

    }

    public function indexAction()
    {
        $users = $this->UsersModel->findAllUsers();
        $departments = new Departments();
        $departments = $departments->findAllDepartments();


        $this->view->render('users/index', 'Users page', ['users' => $users, 'departments' => $departments]);
    }

    public function addAction()
    {
        $user = new Users();
        $validation = new Validate();

        if ($_POST) {
            $user->assign($_POST);
            $validation->check($_POST, Users::$addValidation);
            if ($validation->passed()) {
                $user->save();
                Router::redirect('/users');
            }
        }
    }

    public function detailsAction($id)
    {
        $user = $this->UsersModel->findById($id);
        $this->view->render('users/details', 'User page', $user);
    }

    public function deleteAction($id)
    {
        $this->UsersModel->delete($id["0"]);
        Router::redirect('/users');
    }
}