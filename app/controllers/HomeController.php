<?php


namespace app\controllers;


use core\Controller;
use core\DB;

class HomeController extends Controller
{
    public function __construct($controller, $action)
    {
        parent::__construct($controller, $action);
    }
    public function indexAction()
    {
        $this->view->render('home/index', 'Home page');
    }
}