<?php


namespace core;


use stdClass;

class Model
{
    protected $_db, $_table, $_modelName, $_softDelete = false, $_columnNames = [];
    public $id;

    public function  __construct($table)
    {
        $this->_db = DB::getInstance();
        $this->_table = $table;
        $this->_setTableColumns();
        $this->_modelName = str_replace(' ', '', ucwords(str_replace('_', ' ', $this->_table)));
    }

    protected function _setTableColumns()
    {
        $columns = $this->get_columns();
        foreach ($columns as $column) {
            $columnName = $column->Field;
            $this->_columnNames[] = $column->Field;
            $this->{$columnName} = null;
        }
    }

    public function get_columns()
    {
        return $this->_db->get_columns($this->_table);
    }


    public function find($params = [])
    {
        $path = 'app\models\\'.$this->_modelName;
        $results = [];
        $resultsQuery = $this->_db->find($this->_table, $params);
        if (!$resultsQuery) return [];
        foreach ($resultsQuery as $result) {
            $obj = new $path($this->_table);
            $result = json_decode(json_encode($obj), true);
            $results [] = $result;
        }
        return $results;
    }

    public function findFirst($params = [])
    {
        $path = 'app\models\\'.$this->_modelName;
        $this->_db->findFirst($this->_table, $params);
        $result = new $path($this->_table);
        return json_decode(json_encode($result), true);
    }

    public function findById($id)
    {
        return $this->findFirst(['conditions'=>'id = ?', 'bind' => [$id]]);
    }

    public function save()
    {
        $fields = [];
        foreach ($this->_columnNames as $column) {
            $fields[$column] = $this->$column;
        }
        //determinate to update or insert
        if(property_exists($this, 'id') && $this->id != '') {
            return $this->update($this->id, $fields);
        } else {
            return $this->insert($fields);
        }
    }

    public function data()
    {
        $data = new stdClass();
        foreach ($this->_columnNames as $column) {
            $data->column = $column;
        }
        return $data;
    }

    public function assign($params) {
        if(!empty($params)) {
            foreach ($params as $key => $val) {
                if(in_array($key, $this->_columnNames)) {
                    $this->$key = sanitize($val);
                }
            }
            return true;
        }
        return false;
    }

    public function insert($fields)
    {
        if (empty($fields)) return false;
        return $this->_db->insert($this->_table, $fields);
    }

    public function update($id, $fields)
    {
        if (empty($fields) || $id == '') return false;
        return $this->update($id, $fields);
    }

    public function delete($id)
    {
        if ($id == '' && $this->id == '') return false;
        $id = ($id == '') ? $this->id : $id;

        return $this->_db->delete($this->_table, $id);
    }

    public function query($sql, $bind)
    {
        return $this->_db->query($sql, $bind);
    }

    protected function populateObjData($result) {
        foreach ($result as $key => $val) {
            $this->$key = $val;
        }
    }


}