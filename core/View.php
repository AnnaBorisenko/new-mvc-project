<?php


namespace core;


class View
{
    protected  $_siteTitle = SITE_TITLE, $_outputBuffer, $_layout = DEFAULT_LAYOUT;

    public function __construct()
    {

    }

    public function render($viewName, $siteTitle, $vars = [])
    {
        $this->_siteTitle = $siteTitle;
        extract($vars);
        $viewAry = explode('/', $viewName);
        $viewString = implode(DS, $viewAry);
        if (file_exists(ROOT . DS . 'app' . DS . 'views' . DS .  $viewString . '.php')) {
            ob_start();
            require (ROOT . DS . 'app' . DS . 'views' . DS .  $viewString . '.php');
            $content = ob_get_clean();
            require (ROOT . DS . 'app' . DS . 'views' . DS . 'layouts' . DS . $this->_layout . '.php');
        } else {
            die('The view \"' . $viewName . '\" does not exist.');
        }
    }

    public function siteTitle()
    {
        return $this->_siteTitle;
    }

    public function setSiteTitle($title)
    {
        $this->_siteTitle = $title;
    }
    
    public function setLayout($path)
    {
        $this->_layout = $path;
    }

    public function insert($path)
    {
        include ROOT . DS . 'app' . DS . 'views' . DS . $path . '.php';
    }

    public function partial($group, $partial)
    {
        include ROOT . DS . 'app' . DS . 'views' . DS . $group . DS . 'partials' . DS . $partial . '.php';

    }
}