<?php


namespace core;


class Controller extends Application
{
    protected $_controller, $_action;
    public $view;


    public function __construct($controller, $action)
    {
        parent::__construct();
        $this->_controller = $controller;
        $this->_action = $action;
        $this->view = new View();

    }

    protected function load_model($model)
    {
        $path = 'app\models\\'.ucfirst($model);
        if(class_exists($path)) {
            return $this->{$model.'Model'} = new $path;
        } else {
            echo "Model not found";
        }
    }

}