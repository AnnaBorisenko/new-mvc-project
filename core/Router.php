<?php
namespace core;

use app\controllers\HomeController;

class Router
{

    public static function route($url)
    {
        //controllers
        $controller = (isset($url[0]) && $url[0] != '' ? ucfirst($url[0]).'Controller' : DEFAULT_CONTROLLER);
        $controller_name = $controller;
        array_shift($url);

        //action
        $action = (isset($url[0]) && $url[0] != '' ? $url[0].'Action' : 'indexAction');
        $action_name = $action;
        array_shift($url);

        //params
        $queryParams = $url;
        $path = 'app\controllers\\'.$controller_name;


        if (method_exists($path, $action)) {
            $controller = new $path($action, $queryParams);
            $controller->$action($queryParams);
        } else {
            die('method does not exists');
        }
    }

    public static function redirect($location)
    {
        if(!headers_sent()) {
            header('Location: '.$location);
        } else {
            echo '<script type="text/javascript">';
            echo 'window.location.href="'.$location.'";';
            echo '</script>';
            echo '<noscript>';
            echo '<meta http-equiv="refresh" content="0;url='.$location.'"/>';
            echo '</noscript>';exit;
        }
    }

}